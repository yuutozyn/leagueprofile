const player = {
    songList: require(`./assets/js/player/songList.js`),
    nowPlayingId: 0,
    nowPlayingData: {},
    musicPlayer: document.getElementById("musicPlayer"),
    nowPlayingTitle: document.getElementById("musicPlayerNowPlaying"),
    start() {
        this.updateInfo()
        this.musicPlayer.onended = () => this.nextSong()
    },
    updateInfo() {
        this.nowPlayingData = this.songList[this.nowPlayingId]
        this.musicPlayer.src = this.nowPlayingData.src
        this.nowPlayingTitle.innerHTML = this.nowPlayingData.title        
    },
    nextSong() {
        if(this.nowPlayingId === this.songList.length-1) this.nowPlayingId = 0
        else this.nowPlayingId++
        this.updateInfo()
        this.musicPlayer.play()
    }
}

player.start()