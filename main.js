const { app, BrowserWindow, ipcMain } = require('electron')
const LCUConnector = require('lcu-connector')
const connector = new LCUConnector()

const Axios = require('axios').default
const https = require('https')

// Server -----------------------------
const server = require('./server.js')
const Server = new server()
Server.init()
// Server -----------------------------

const Schemas = require("./core/Schemas/index.js")

let mainWindow
let axios
let Summoner
let Client

connector.on('connect', async(data) => {
    const AuthToken = `Basic ${Buffer.from(`${data.username}:${data.password}`, 'utf8').toString('base64')}`
    axios = Axios.create({
        baseURL: `${data.protocol}://${data.address}:${data.port}`,
        timeout: 5000,
        headers: {
            'Authorization': AuthToken
        },
        httpsAgent: new https.Agent({  
            rejectUnauthorized: false
        })
    })

    //await refreshPlayerData()
});
 
async function refreshPlayerData() {
    await axios.get('/lol-summoner/v1/current-summoner').then(async(response) => {
        Summoner = response.data
        Summoner.existed = false
        if(!Summoner) return
        let leaguePlayerExists = await Schemas.LeaguePlayer.findOne({ id: Summoner.puuid })
        if(!leaguePlayerExists) {
            const TempData = new Schemas.LeaguePlayer({
                id: Summoner.puuid
            })
            TempData.save(function (err) {
                if (err) return console.log(err)
            })
            leaguePlayerExists = await Schemas.LeaguePlayer.findOne({ id: Summoner.puuid })
            Client = leaguePlayerExists
        } else {
            Summoner.existed = true
            Client = leaguePlayerExists
        }
    })
}

connector.start();

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 1000,
        height: 620,
        resizable: false,
        frame: false,
        movable: true,
        icon: `${__dirname}/src/assets/img/icon.png`,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            backgroundThrottling: false
        }
    })

    mainWindow.loadURL(`file://${__dirname}/src/index.html`)
})

ipcMain.on('summonerData', async(event, arg) => {
    await refreshPlayerData()
    if(!Client) return
    event.returnValue = {SummonerData: Summoner, ClientData: Client.toJSON()}
})

ipcMain.on('submitDescription', (event, arg) => {
    axios.put('/lol-chat/v1/me', {"statusMessage": arg})
})

ipcMain.on('changeClientBackground', async(event, arg) => {
    await Client.updateOne({
        background: arg
    })
})

ipcMain.on('toggleAutoAcceptQueue', async(event, arg) => {
    await Client.updateOne({
        "options.autoQueueAccept": arg
    })
})

ipcMain.on('changeLeagueBackground', (event, arg) => {
    axios.post('/lol-summoner/v1/current-summoner/summoner-profile/', { key: "backgroundSkinId", value: arg })
})

function autoAcceptQueue() {
    setInterval(() => {
        if(Client) {
            const auto_accept = Client.toJSON().options.autoQueueAccept
            if(auto_accept === false) return
            axios.get("/lol-matchmaking/v1/ready-check").then((response) => {
                const res = response.data
                if(res.state === "InProgress") {
                    axios.post("/lol-matchmaking/v1/ready-check/accept", {})
                }
            }).catch((err) => {
                return
            })
        }
    }, 1000)
}

autoAcceptQueue()