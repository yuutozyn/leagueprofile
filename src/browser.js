const { ipcRenderer } = require('electron')
const axios = require('axios')

let app_version = "League Profile v1.2"
document.getElementById("app_version").innerHTML = app_version

// Test zone
let game_version
axios.get('https://ddragon.leagueoflegends.com/api/versions.json').then((response) => {
    game_version = response.data[0]

    axios.get(`http://ddragon.leagueoflegends.com/cdn/${game_version}/data/en_US/champion.json`).then((response) => {
        const championList = response.data.data
        Object.keys(championList).forEach((champion) => {
            const championIcon = `http://ddragon.leagueoflegends.com/cdn/${game_version}/img/champion/${champion}.png`
            // championBackgroundSkins
            document.getElementById("championBackgroundGrid").innerHTML += ` 
                <div class="col-md-3 col-xl-2">
                    <div class="block block-rounded">
                        <div class="block-header" style="background-color: #F35;">
                            <h3 class="block-title text-center text-white">${champion}</h3>
                        </div>
                        <div class="block-content text-center text-grey" style="background-color: #2b2b3b; color: #CCE;">
                            <span>
                                <img class="img-avatar" src=${championIcon} alt="">
                            </span>
                            <button type="button" class="btn btn-danger mt-4 mb-4" style="background-color: #F35" onClick="openSkinsModal('${champion}')">Selecionar</button>
                        </div>
                    </div>
                </div>
            `
        })
    })

})

function openSkinsModal(champion) {
    axios.get(`http://ddragon.leagueoflegends.com/cdn/${game_version}/data/en_US/champion/${champion}.json`).then((response) => {
        const championSkins = response.data.data[`${champion}`].skins
        document.getElementById("championBackgroundSkinGrid").innerHTML = ""
        championSkins.forEach((skin) => {
            document.getElementById("championBackgroundSkinGrid").innerHTML += `
                <div class="col-md-2 animated fadeIn mb-2">
                    <div class="options-container">
                        <img class="img-fluid options-item" src="http://ddragon.leagueoflegends.com/cdn/img/champion/loading/${champion}_${skin.num}.jpg" alt="">
                        <div class="options-overlay bg-black-75">
                            <div class="options-overlay-content">
                                <h3 class="h4 text-white mb-2">${skin.name}</h3>
                                <a class="btn btn-sm btn-danger text-white" style="background-color: #F02040" onClick="changeLeagueBackground(${skin.id})">
                                    <i class="fa fa-pencil-alt text-white mr-1"></i> Selecionar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            `
        })
        jQuery('#champion-skins-modal').modal('show')
    })
}

function changeLeagueBackground(skinId) {
    if(!clientData) return
    ipcRenderer.send('changeLeagueBackground', skinId)

    Swal.fire({
        title: 'Pronto!',
        text: `Mudei o seu wallpaper`,
        icon: 'success',
        confirmButtonText: 'Legal'
    })
}
// Test zone



var clientData

function updateProfile() {
    try {
        const {SummonerData,ClientData} = ipcRenderer.sendSync('summonerData', 'retrieveData')
        clientData = ClientData

        // Summoner Data
        document.getElementById("username").innerHTML = SummonerData.displayName
        document.getElementById("avatar").src = `http://ddragon.leagueoflegends.com/cdn/10.22.1/img/profileicon/${SummonerData.profileIconId}.png`
        document.getElementById("level").innerHTML = `Lvl. ${SummonerData.summonerLevel}`
        
        // Client Data
        if(document.getElementById("clientBackground").style.backgroundImage !== ClientData.background) document.getElementById("clientBackground").style.backgroundImage = `url(${ClientData.background})`
        document.getElementById("clientDescription").innerHTML = ClientData.description
        document.getElementById("clientConfigAutoAcceptQueue").checked = ClientData.options.autoQueueAccept

        if(!SummonerData.existed) {
            Swal.fire({
                title: 'Bem-vindo (a)!',
                text: `Primeira vez usando o LeagueProfile, né? Criei um perfil para você!`,
                icon: 'success',
                confirmButtonText: 'Obrigado'
            })
        }

    } catch(error) {
        console.error(error)
    }
}

function submitDescription() {
    const description = document.getElementById("leagueDescription").value
    if(description.length < 1) return
    ipcRenderer.send('submitDescription', description)
    Swal.fire({
        title: 'Pronto!',
        text: `Mudei o seu texto`,
        icon: 'success',
        confirmButtonText: 'Legal'
    })
    document.getElementById("leagueDescription").value = ""
}

function changeClientBackground(bgId) {
    if(!clientData) return
    const bgs = {
        0: "https://i.pinimg.com/originals/37/ab/7d/37ab7de282f402564dcfabc578d16cf1.gif",
        1: "https://lol-stats.net/uploads/YwJ3FiEIkbQC0e8Yshtr2MtXii5Hh9NgwSiWGkr1.jpeg",
        2: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/14f93389-f0c8-4f1c-a732-2a8efb4a8867/dbmoblw-d6f63d44-1d3f-44ed-a50e-e3a85b4b25f9.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvMTRmOTMzODktZjBjOC00ZjFjLWE3MzItMmE4ZWZiNGE4ODY3XC9kYm1vYmx3LWQ2ZjYzZDQ0LTFkM2YtNDRlZC1hNTBlLWUzYTg1YjRiMjVmOS5naWYifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.b0ITLfWy8pLibkwX4n7r2VqFdvdaWBcpH7pw4mCpkCI"
    }

    ipcRenderer.send('changeClientBackground', bgs[bgId])
    Swal.fire({
        title: 'Pronto!',
        text: `Mudei o wallpaper do cliente`,
        icon: 'success',
        confirmButtonText: 'Legal'
    })
}

function toggleAutoAcceptQueue() {
    const check = document.getElementById("clientConfigAutoAcceptQueue").checked
    if(!clientData) return
    ipcRenderer.send('toggleAutoAcceptQueue', check)
}

// Auto Update
setInterval(async() => {
    updateProfile()
}, 5000)