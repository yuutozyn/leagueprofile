const mongoose = require('mongoose')

const Server = class Server {
    constructor(version) {
        this.version = version || "Development"
    }

    init() {
        console.log("[MONGO SERVER] Connecting...")
        mongoose.connect('mongodb+srv://lolprofileengine:lolprofileengine@cluster0.ogqd1.mongodb.net/Development?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true})
        this.connection = mongoose.connection
        this.initHandlers()
    }

    initHandlers() {
        this.connection.on('error', (error) => { this.errorHandler(error) })
        this.connection.on('open', () => {
            console.log("[MONGO SERVER] Connected!")
        })
    }

    errorHandler(error) {
        console.log("[MONGO SERVER] Error!\n", error)
    }
}

module.exports = Server