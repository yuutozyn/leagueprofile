const mongoose = require('mongoose')
const Schema = mongoose.Schema;
 
// ID = PUUID
const LeaguePlayer = new Schema({
    id: String,
    background: { type: String, default: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/14f93389-f0c8-4f1c-a732-2a8efb4a8867/dbmoblw-d6f63d44-1d3f-44ed-a50e-e3a85b4b25f9.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvMTRmOTMzODktZjBjOC00ZjFjLWE3MzItMmE4ZWZiNGE4ODY3XC9kYm1vYmx3LWQ2ZjYzZDQ0LTFkM2YtNDRlZC1hNTBlLWUzYTg1YjRiMjVmOS5naWYifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.b0ITLfWy8pLibkwX4n7r2VqFdvdaWBcpH7pw4mCpkCI" },
    description: { type: String, default: "Aww, parece que não tem nada aqui :c Mude isso nas configurações!"},
    badges: Array,
    options: {
        autoQueueAccept: { type: Boolean, default: false},
        autoLockChampion: { enabled: { type: Boolean, default: false}, champion: String}
    }
});

module.exports = mongoose.model('LeaguePlayer', LeaguePlayer)