module.exports = [
    {
        title: "THE BADDEST",
        src: "assets/audio/TheBaddest.mp3"
    },
    {
        title: "MORE",
        src: "assets/audio/MORE.mp3"
    },
    {
        title: "DRUM GO DRUM",
        src: "assets/audio/drumgodrum.mp3"
    },
    {
        title: "VILLAIN",
        src: "assets/audio/villain.mp3"
    },
    {
        title: "I'LL SHOW YOU",
        src: "assets/audio/illshowyou.mp3"
    },
]